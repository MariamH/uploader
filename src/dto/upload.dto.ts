import { ApiProperty } from '@nestjs/swagger';
import { FastifyReply, FastifyRequest } from 'fastify';

export class UploadDto {
  @ApiProperty({ description: 'Request', required: true })
  req: any;

  @ApiProperty({ description: 'Reply', required: true })
  reply: FastifyReply<any>;
}
