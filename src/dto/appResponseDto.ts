import { ApiProperty } from '@nestjs/swagger';

export class AppResponseDto {
  @ApiProperty({ description: 'Message describing request result' })
  message: string;

  @ApiProperty({ description: 'Uploaded file url' })
  url: null | string = null;

  constructor(dto: { message: string; url: null | string }) {
    this.message = dto.message;
    this.url = dto.url;
  }
}
