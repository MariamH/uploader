import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';

export class UploadToAwsDto {
  @ApiProperty({ description: 'File mimeType', required: true })
  @IsString({ message: 'File mimeType should be a string' })
  mimeType: string;

  @ApiProperty({ description: 'File buffer', required: true })
  @IsString({ message: 'File buffer should be a string' })
  path: string;

  @ApiProperty({ description: 'File name', required: true })
  @IsString({ message: 'File name should be a string' })
  fileName: string;
}
