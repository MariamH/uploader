import {
  Controller,
  HttpCode,
  HttpStatus,
  Post,
  Req,
  Res,
} from '@nestjs/common';
import { AppService } from './app.service';
import {
  ApiBody,
  ApiConsumes,
  ApiOperation,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { FastifyReply, FastifyRequest } from 'fastify';
import { AppResponseDto } from '../../dto/appResponseDto';

@ApiTags('uploader')
@Controller('upload')
export class AppController {
  constructor(private readonly appService: AppService) {}

  @ApiOperation({
    summary: 'Upload file to AWS',
    description: 'Upload received file to AWS',
  })
  @ApiResponse({
    status: 201,
    description: 'Success',
    type: AppResponseDto,
  })
  @ApiResponse({
    status: 400,
    description: 'Provided data is invalid',
    type: AppResponseDto,
  })
  @ApiResponse({
    status: 500,
    description: 'Internal server error occurred',
    type: AppResponseDto,
  })
  @ApiConsumes('multipart/form-data')
  @ApiBody({
    schema: {
      type: 'object',
      properties: {
        file: {
          type: 'string',
          format: 'binary',
        },
      },
    },
  })
  @Post()
  @HttpCode(HttpStatus.OK)
  async upload(
    @Req() req: FastifyRequest,
    @Res() reply: FastifyReply<any>,
  ): Promise<AppResponseDto> {
    return await this.appService.upload({ req, reply });
  }
}
