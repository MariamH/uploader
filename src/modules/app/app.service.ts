import {
  BadRequestException,
  HttpStatus,
  Injectable,
  Logger,
} from '@nestjs/common';
import { AwsService } from '../../shared/services/aws.service';
import { AppResponseDto } from '../../dto/appResponseDto';
import { join } from 'path';
import * as stream from 'stream';
import * as fs from 'fs';
import * as util from 'util';
import { UploadDto } from '../../dto/upload.dto';
import { FastifyReply } from 'fastify';

@Injectable()
export class AppService {
  private readonly logger = new Logger('AppService');
  private reply: FastifyReply<any>;
  constructor(private readonly awsService: AwsService) {}

  async upload(uploadDto: UploadDto): Promise<any> {
    const { req, reply } = uploadDto;
    if (!req.isMultipart()) {
      reply.code(HttpStatus.UNPROCESSABLE_ENTITY).send(
        new BadRequestException(
          new AppResponseDto({
            message: 'Incorrect file type',
            url: null,
          }),
        ),
      );
    }
    this.reply = reply;
    await req.multipart(this.handler.bind(this), () => {});
  }

  private async handler(
    field: string,
    file: any,
    filename: string,
    encoding: string,
    mimetype: string,
  ): Promise<any> {
    const directory = join(__dirname, `uploads`);
    if (!fs.existsSync(directory)) {
      fs.mkdirSync(directory);
    }

    const pipeline = util.promisify(stream.pipeline);
    const writeStream = fs.createWriteStream(`${directory}/${filename}`);
    try {
      await pipeline(file, writeStream);

      const url = await this.awsService.uploadToAws({
        fileName: filename.toString(),
        path: `${directory}/${filename}`,
        mimeType: mimetype,
      });
      this.reply.code(HttpStatus.OK).send(
        new AppResponseDto({
          message: 'Success',
          url,
        }),
      );
    } catch (err) {
      this.logger.error(err.message);
      this.reply.code(HttpStatus.INTERNAL_SERVER_ERROR).send(
        new AppResponseDto({
          message: err.message,
          url: null,
        }),
      );
    } finally {
      if (fs.existsSync(directory)) {
        fs.rmdirSync(directory, { recursive: true });
      }
    }
  }
}
