import { NestFactory } from '@nestjs/core';
import { AppModule } from './modules/app/app.module';
import {
  FastifyAdapter,
  NestFastifyApplication,
} from '@nestjs/platform-fastify';
import { SharedModule } from './shared/shared.module';
import { ValidationPipe } from '@nestjs/common';
import { setupSwagger } from './setup-swagger';
import { ApiConfigService } from './shared/services/api-config.service';
import multipart from 'fastify-multipart';

async function bootstrap() {
  const app = await NestFactory.create<NestFastifyApplication>(
    AppModule,
    new FastifyAdapter({ logger: true }),
  );

  const configService = app.select(SharedModule).get(ApiConfigService);
  app.useGlobalPipes(new ValidationPipe());
  const port = configService.appConfig.port;

  if (configService.documentationEnabled) {
    setupSwagger(app);
  }

  await app.register(multipart);
  await app.listen(port);
}

bootstrap();
